/*-
 * Copyright (c) 1996
 *	Rob Zimmermann.  All rights reserved.
 * Copyright (c) 1996
 *	Keith Bostic.  All rights reserved.
 *
 * See the LICENSE file for redistribution information.
 */

#include "config.h"

#include <sys/queue.h>

#include <bitstring.h>
#include <stdio.h>
#include <stdarg.h>

#include "common.h"

#ifdef TRACE

static FILE *tfp;

/*
 * vtrace_end --
 *	End tracing.
 *
 * PUBLIC: void vtrace_end(void);
 */
void
vtrace_end(void)
{
	if (tfp != NULL && tfp != stderr)
		fclose(tfp);
}

/*
 * vtrace_init --
 *	Initialize tracing.
 *
 * PUBLIC: void vtrace_init(char *);
 */
void
vtrace_init(char *name)
{
	if (name == NULL || (tfp = fopen(name, "w")) == NULL)
		tfp = stderr;
	vtrace("\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\nTRACE\n");
}

/*
 * vtrace --
 *	Debugging trace routine.
 *
 * PUBLIC: void vtrace(const char *, ...);
 */
void
vtrace(const char *fmt, ...)
{
	va_list ap;

	if (tfp == NULL)
		vtrace_init(NULL);

	va_start(ap, fmt);
	vfprintf(tfp, fmt, ap);
	va_end(ap);

	fflush(tfp);
}
#endif
