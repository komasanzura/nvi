/*-
 * Copyright (c) 1991, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 * Copyright (c) 1991, 1993, 1994, 1995, 1996
 *	Keith Bostic.  All rights reserved.
 *
 * See the LICENSE file for redistribution information.
 */

/*
 * Avoid include sys/types.h after definition of pgno_t
 */
#include <sys/types.h>
#include <sys/queue.h>
#include <bitstring.h>

/*
 * Pseudo-local includes.  These are files that are unlikely to exist
 * on most machines to which we're porting vi, and we want to include
 * them in a very specific order, regardless.
 */
#include "db.h"
#include <regex.h>

/*
 * Forward structure declarations.  Not pretty, but the include files
 * are far too interrelated for a clean solution.
 */
typedef struct _cb		CB;
typedef struct _csc		CSC;
typedef struct _conv	    	CONV;
typedef struct _conv_win    	CONVWIN;
typedef struct _event		EVENT;
typedef struct _excmd		EXCMD;
typedef struct _exf		EXF;
typedef struct _fref		FREF;
typedef struct _gs		GS;
typedef struct _lmark		LMARK;
typedef struct _mark		MARK;
typedef struct _msg		MSGS;
typedef struct _option		OPTION;
typedef struct _optlist		OPTLIST;
typedef struct _scr		SCR;
typedef struct _script		SCRIPT;
typedef struct _seq		SEQ;
typedef struct _tag		TAG;
typedef struct _tagf		TAGF;
typedef struct _tagq		TAGQ;
typedef struct _text		TEXT;
typedef struct _win		WIN;

/* Autoindent state. */
typedef enum { C_NOTSET, C_CARATSET, C_NOCHANGE, C_ZEROSET } carat_t;

/* Busy message types. */
typedef enum { BUSY_ON = 1, BUSY_OFF, BUSY_UPDATE } busy_t;

/*
 * Routines that return a confirmation return:
 *
 *	CONF_NO		User answered no.
 *	CONF_QUIT	User answered quit, eof or an error.
 *	CONF_YES	User answered yes.
 */
typedef enum { CONF_NO, CONF_QUIT, CONF_YES } conf_t;

/* Directions. */
typedef enum { NOTSET, FORWARD, BACKWARD } dir_t;

/* Line operations. */
typedef enum { LINE_APPEND, LINE_DELETE, LINE_INSERT, LINE_RESET } lnop_t;

/* Lock return values. */
typedef enum { LOCK_FAILED, LOCK_SUCCESS, LOCK_UNAVAIL } lockr_t;

/* Sequence types. */
typedef enum { SEQ_ABBREV, SEQ_COMMAND, SEQ_INPUT } seq_t;

/*
 * Local includes.
 */
#include "key.h"		/* Required by args.h. */
#include "args.h"		/* Required by options.h. */
#include "options.h"		/* Required by screen.h. */

#include "msg.h"		/* Required by gs.h. */
#include "cut.h"		/* Required by gs.h. */
#include "seq.h"		/* Required by screen.h. */
#include "util.h"		/* Required by ex.h. */
#include "mark.h"		/* Required by gs.h. */
#include "conv.h"		/* Required by ex.h and screen.h */
#include "../ex/ex.h"		/* Required by gs.h. */
#include "gs.h"			/* Required by screen.h. */
#include "log.h"		/* Required by screen.h */
#include "screen.h"		/* Required by exf.h. */
#include "exf.h"
#include "mem.h"

/* api.c */
SCR *api_fscreen(int, char *);
int api_aline(SCR *, db_recno_t, char *, size_t);
int api_extend(SCR *, db_recno_t);
int api_dline(SCR *, db_recno_t);
int api_gline(SCR *, db_recno_t, CHAR_T **, size_t *);
int api_iline(SCR *, db_recno_t, CHAR_T *, size_t);
int api_lline(SCR *, db_recno_t *);
int api_sline(SCR *, db_recno_t, CHAR_T *, size_t);
int api_getmark(SCR *, int, MARK *);
int api_setmark(SCR *, int, MARK *);
int api_nextmark(SCR *, int, char *);
int api_getcursor(SCR *, MARK *);
int api_setcursor(SCR *, MARK *);
void api_emessage(SCR *, char *);
void api_imessage(SCR *, char *);
int api_edit(SCR *, char *, SCR **, int);
int api_escreen(SCR *);
int api_swscreen(SCR *, SCR *);
int api_map(SCR *, char *, char *, size_t);
int api_unmap(SCR *, char *);
int api_opts_get(SCR *, CHAR_T *, char **, int *);
int api_opts_set(SCR *, CHAR_T *, char *, u_long, int);
int api_run_str(SCR *, char *);
TAGQ * api_tagq_new(SCR*, char*);
void api_tagq_add(SCR*, TAGQ*, char*, char *, char *);
int api_tagq_push(SCR*, TAGQ**);
void api_tagq_free(SCR*, TAGQ*);

/* cut.c */
int cut(SCR *, CHAR_T *, MARK *, MARK *, int);
int cut_line(SCR *, db_recno_t, size_t, size_t, CB *);
void cut_close(WIN *);
TEXT *text_init(SCR *, const CHAR_T *, size_t, size_t);
void text_lfree(TEXTH *);
void text_free(TEXT *);

/* db.c */
int db_eget(SCR *, db_recno_t, CHAR_T **, size_t *, int *);
int db_get(SCR *, db_recno_t, u_int32_t, CHAR_T **, size_t *);
int db_delete(SCR *, db_recno_t);
int db_append(SCR *, int, db_recno_t, CHAR_T *, size_t);
int db_insert(SCR *, db_recno_t, CHAR_T *, size_t);
int db_set(SCR *, db_recno_t, CHAR_T *, size_t);
int db_exist(SCR *, db_recno_t);
int db_last(SCR *, db_recno_t *);
void db_err(SCR *, db_recno_t);
int scr_update(SCR *sp, db_recno_t lno, lnop_t op, int current);
void update_cache(SCR *sp, lnop_t op, db_recno_t lno);
int line_insdel(SCR *sp, lnop_t op, db_recno_t lno);

/* delete.c */
int del(SCR *, MARK *, MARK *, int);

/* exf.c */
FREF *file_add(SCR *, char *);
int file_init(SCR *, FREF *, char *, int);
int file_end(SCR *, EXF *, int);
int file_write(SCR *, MARK *, MARK *, char *, int);
int file_m1(SCR *, int, int);
int file_m2(SCR *, int);
int file_m3(SCR *, int);
int file_aw(SCR *, int);
void set_alt_name(SCR *, char *);
lockr_t file_lock(SCR *, char *, int *, int, int);

/* gs.c */
GS * gs_init(char*);
WIN * gs_new_win(GS *gp);
int win_end(WIN *wp);
void gs_end(GS *);

/* key.c */
int v_key_init(SCR *);
void v_key_ilookup(SCR *);
size_t v_key_len(SCR *, ARG_CHAR_T);
u_char *v_key_name(SCR *, ARG_CHAR_T);
int v_key_val(SCR *, ARG_CHAR_T);
int v_event_push(SCR *, EVENT *, CHAR_T *, size_t, u_int);
int v_event_get(SCR *, EVENT *, int, u_int32_t);
void v_event_err(SCR *, EVENT *);
int v_event_flush(SCR *, u_int);

/* log.c */
int log_init(SCR *, EXF *);
int log_end(SCR *, EXF *);
int log_cursor(SCR *);
int log_line(SCR *, db_recno_t, u_int);
int log_mark(SCR *, LMARK *);
int log_backward(SCR *, MARK *);
int log_setline(SCR *);
int log_forward(SCR *, MARK *);

/* main.c */
int editor(WIN *, int, char *[]);

/* mark.c */
int mark_init(SCR *, EXF *);
int mark_end(SCR *, EXF *);
int mark_get(SCR *, ARG_CHAR_T, MARK *, mtype_t);
int mark_set(SCR *, ARG_CHAR_T, MARK *, int);
int mark_insdel(SCR *, lnop_t, db_recno_t);

/* msg.c */
void msgq(SCR *, mtype_t, const char *, ...);
void msgq_wstr(SCR *, mtype_t, CHAR_T *, char *);
void msgq_str(SCR *, mtype_t, char *, char *);
void mod_rpt(SCR *);
void msgq_status(SCR *, db_recno_t, u_int);
int msg_open(SCR *, char *);
void msg_close(GS *);
const char *msg_cmsg(SCR *, cmsg_t, size_t *);
const char *msg_cat(SCR *, const char *, size_t *);
char *msg_print(SCR *, const char *, int *);

/* nothread.c | thread.c */
void thread_init(GS *gp);

/* options.c */
int opts_init(SCR *, int *);
int opts_set(SCR *, ARGS *[], char *);
int o_set(SCR *, int, u_int, char *, u_long);
int opts_empty(SCR *, int, int);
void opts_dump(SCR *, enum optdisp);
int opts_save(SCR *, FILE *);
OPTLIST const *opts_search(CHAR_T *);
void opts_nomatch(SCR *, CHAR_T *);
int opts_copy(SCR *, SCR *);
void opts_free(SCR *);

/* options_f.c */
int f_altwerase(SCR *, OPTION *, char *, u_long *);
int f_columns(SCR *, OPTION *, char *, u_long *);
int f_lines(SCR *, OPTION *, char *, u_long *);
int f_lisp(SCR *, OPTION *, char *, u_long *);
int f_msgcat(SCR *, OPTION *, char *, u_long *);
int f_paragraph(SCR *, OPTION *, char *, u_long *);
int f_print(SCR *, OPTION *, char *, u_long *);
int f_readonly(SCR *, OPTION *, char *, u_long *);
int f_recompile(SCR *, OPTION *, char *, u_long *);
int f_reformat(SCR *, OPTION *, char *, u_long *);
int f_section(SCR *, OPTION *, char *, u_long *);
int f_ttywerase(SCR *, OPTION *, char *, u_long *);
int f_w300(SCR *, OPTION *, char *, u_long *);
int f_w1200(SCR *, OPTION *, char *, u_long *);
int f_w9600(SCR *, OPTION *, char *, u_long *);
int f_window(SCR *, OPTION *, char *, u_long *);
int f_encoding(SCR *, OPTION *, char *, u_long *);

/* put.c */
int put(SCR *, CB *, CHAR_T *, MARK *, MARK *, int);

/* recover.c */
int rcv_tmp(SCR *, EXF *, char *);
int rcv_init(SCR *);
int rcv_sync(SCR *, u_int);
int rcv_list(SCR *);
int rcv_read(SCR *, FREF *);

/* screen.c */
int screen_init(GS *, SCR *, SCR **);
int screen_end(SCR *);
SCR *screen_next(SCR *);

/* search.c */
int f_search(SCR *, MARK *, MARK *, CHAR_T *, size_t, CHAR_T **, u_int);
int b_search(SCR *, MARK *, MARK *, CHAR_T *, size_t, CHAR_T **, u_int);
void search_busy(SCR *, busy_t);

/* seq.c */
int seq_set(SCR *, CHAR_T *, size_t, CHAR_T *, size_t, CHAR_T *, size_t, seq_t, int);
int seq_delete(SCR *, CHAR_T *, size_t, seq_t);
int seq_mdel(SEQ *);
SEQ *seq_find(SCR *, SEQ **, EVENT *, CHAR_T *, size_t, seq_t, int *);
void seq_close(GS *);
int seq_dump(SCR *, seq_t, int);
int seq_save(SCR *, FILE *, char *, seq_t);
int e_memcmp(CHAR_T *, EVENT *, size_t);

/* trace.c */
void vtrace_end(void);
void vtrace_init(char *);
void vtrace(const char *, ...);

/* util.c */
void *binc(SCR *, void *, size_t *, size_t);
int nonblank(SCR *, db_recno_t, size_t *);
char *tail(char *);
char *v_strdup(SCR *, const char *, size_t);
CHAR_T *v_wstrdup(SCR *, const CHAR_T *, size_t);
enum nresult nget_uslong(SCR *, u_long *, const CHAR_T *, CHAR_T **, int);
enum nresult nget_slong(SCR *, long *, const CHAR_T *, CHAR_T **, int);
