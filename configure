#!/bin/sh

# This configure script was written by Brian Callahan <bcallah@openbsd.org>
# and released into the Public Domain.
# It was restructured, reformatted and expanded by KomasanZura

trap "rm -f conftest conftest.o conftest.c" EXIT

# Do a compilation test using a sample C source file using $cc as the
# compiler. The sample file must exist before calling this function and
# its name must be "conftest.c"
testcc() {
	if [ -n "$cc" ]
	then
		$cc $cflags $ldflags -oconftest conftest.c $libs >/dev/null 2>&1
	else
		false
	fi
}

# Test some possible compilers until a working one is found
# Note: Not all of these compilers have been tested by me.
findcc() {
	echo "int main(void) { return 0; }" >conftest.c
	for compiler in "$CC" cc gcc clang pcc xlc cparser
	do
		cc="$compiler"
		if testcc
		then
			return 0
		fi
	done
	cc=
	return 1
}

# If an error happens
abend() {
	echo "An error occurred. Configuration aborted."
	rm -f conftest.c conftest.o conftest include/config.h Makefile
	exit 1
}

# Option variables and their default values
prefix="/usr/local"
bindir="$prefix/bin"
datadir="$prefix/share"
mandir="$datadir/man"
bindirset="no"
datadirset="no"
mandirset="no"
progprefix=""
enable_static="no"
enable_strip="no"
enable_widechar="no"
enable_threads="no"
with_regex="bundled"

# Option handling
for opt
do
	case "$opt" in
	--prefix=*)
		prefix="`echo $opt | cut -d= -f2`"
	;;
	--bindir=*)
		bindir="`echo $opt | cut -d= -f2`"
		bindirset="yes"
	;;
	--datadir=*|--datarootdir=*)
		datadir="`echo $opt | cut -d= -f2`"
		datadirset="yes"
	;;
	--mandir=*)
		mandir="`echo $opt | cut -d= -f2`"
		mandirset="yes"
	;;
	--enable-static|--enable-static=y*)
		enable_static="yes"
	;;
	--disable-static|--enable-static=n*)
		enable_static="no"
	;;
	--enable-strip|--enable-strip=y*)
		enable_strip="yes"
	;;
	--disable-strip|--enable-strip=n*)
		enable_strip="no"
	;;
	--enable-widechar|--enable-widechar=y*)
		enable_widechar="yes"
	;;
	--disable-widechar|--enable-widechar=n*)
		enable_widechar="no"
	;;
	--enable-threads|--enable-threads=y*)
		enable_threads="yes"
	;;
	--disable-threads|--enable-threads=n*)
		enable_threads="no"
	;;
	--with-regex=*)
		with_regex="`echo $opt | cut -d= -f2`"
		if [ "system" != "$with_regex" -a "bundled" != "$with_regex" ]
		then
			echo "Invalid regex option: $with_regex"
			abend
		fi
	;;
	--program-prefix=*)
		progprefix="`echo $opt | cut -d= -f2`"
	;;
	--help|-h)
		echo "Usage: $0 [options]"
		echo ""
		echo "Options: (default value in brackets)"
		printf "  --help or -h	         "
		echo "Display this help message"
		printf "  --prefix=PREFIX        "
		echo "Top level install directory is PREFIX [$prefix]"
		printf "  --bindir=BINDIR        "
		echo "Install directory for executables [\$PREFIX/bin]"
		printf "  --datadir=DATADIR      "
		echo "Base directory for arch-agnostic files [\$PREFIX/share]"
		printf "  --mandir=MANDIR        "
		echo "Manual pages are installed to MANDIR [\$DATADIR/man]"
		printf "  --program-prefix=STR   "
		echo "Prepend executable names with STR  []"
		printf "  --enable-widechar      "
		echo "Enable experimental wide character support [no]"
		printf "  --enable-threads       "
		echo "Enable experimental pthreads support [no]"
		printf "  --with-regex=system    "
		echo "Use system regex library instead of bundled [bundled]"
		printf "  --enable-static        "
		echo "Statically link executables [no]"
		printf "  --enable-strip         "
		echo "Strip executables when installing [no]"
		exit
	;;
	*)
		echo "Unknown option: $opt"
		abend
	;;
	esac
done

# Set bindir, datadir, and mandir again in case prefix has changed
if [ "$bindirset" = "no" ]
then
	bindir="$prefix/bin"
fi

if [ "$datadirset" = "no" ]
then
	datadir="$prefix/share"
fi

if [ "$mandirset" = "no" ]
then
	mandir="$datadir/man"
fi

if [ "$enable_strip" = "yes" ] 
then
	stripflag="-s"
fi

# Set CFLAGS and LDFLAGS then start searching for a compiler.
# We accept CFLAGS, LDFLAGS and LIBS from the environment if set
cflags="-Iinclude $CFLAGS"
ldflags="$LDFLAGS"
libs="$LIBS"

if [ "$with_regex" = "bundled" ]
then
	cflags="-Iregex $cflags"
fi

if [ "$enable_static" = "yes" ] 
then
	ldflags="-static $ldflags"
fi

printf "checking for C compiler... "
if findcc
then
	echo "$cc"
else
	echo "none found"
	echo "Please install a C compiler and re-run configure."
	abend
fi

printf "checking for OS... "
os="`uname -s`"
case "$os" in
Linux|CYGWIN*)
	cflags="-D_GNU_SOURCE $cflags"
;;
*)
	cflags="-D_XOPEN_SOURCE=700 $cflags"
;;
esac
echo "$os"

have_flock="no"
need_libbsd="no"

printf "checking for flock... "
cat <<"EOF" >conftest.c
#include <sys/file.h>
int main(void) { flock(-1, 0); return 0; }
EOF
if testcc
then
	have_flock="yes"
fi
echo "$have_flock"

printf "checking for bitstring.h... "
cat <<"EOF" >conftest.c
#include <stdlib.h>
#include <bitstring.h>
int main(void) { bitstr_t bit_decl(x, 7); }
EOF
if testcc
then
	echo "yes"
else
	echo "no"
	need_libbsd="yes"
fi

if [ "$need_libbsd" = "yes" ]
then
	printf "trying with libbsd... "
	if pkg-config libbsd-overlay >/dev/null 2>&1
	then
		cflags="`pkg-config --cflags libbsd-overlay` $cflags"
		libs="`pkg-config --libs libbsd-overlay` $libs"
		if testcc
		then
			echo "success"
		else
			echo "failed"
			echo "Could not find a suitable bitstring.h in your system."
			abend
		fi
	else
		echo "not found"
		echo "The required bsd library (libbsd) was not found."
		abend
	fi
fi

printf "checking for iconv... "
cat <<"EOF" >conftest.c
#include <stddef.h>
#include <iconv.h>
int main(void) { iconv_t x; iconv(x, NULL, NULL, NULL, NULL); return 0; }
EOF
if testcc
then
	echo "builtin"
else
	libs="-liconv $libs"
	if testcc
	then
		echo "yes"
	else
		echo "no"
		echo "The required iconv library was not found"
		abend
	fi
fi

printf "checking for curses... "
if ncursesw6-config --version >/dev/null 2>&1
then
	cflags="`ncursesw6-config --cflags` $cflags"
	libs="`ncursesw6-config --libs` $libs"
else
	libs="-lcurses $libs"
fi
cat <<"EOF" >conftest.c
#include <stddef.h>
#include <stdio.h>
#include <curses.h>
#include <term.h>
int main(void) { waddnwstr(NULL, L"test", -1); return 0; }
EOF
if testcc
then
	echo "yes"
else
	echo "no"
	echo "No curses installation was found."
	abend
fi

printf "checking for db-5.3... "
libs="-ldb-5.3 $libs"
cat <<"EOF" >conftest.c
#include <db.h>
int main(void) { db_create(NULL, NULL, 0); return 0; }
EOF
if testcc
then
	echo "yes"
else
	echo "no"
	echo "No db installation was found."
	abend
fi

if [ "$enable_threads" = "yes" ]
then
	printf "checking for pthread... "
	libs="-lpthread $libs"
	cat <<"EOF" >conftest.c
#include <pthread.h>
int main(void) { pthread_self(); return 0; }
EOF
	if testcc
	then
		echo "yes"
	else
		echo "no"
		echo "Thread support was requested but the pthread library was not found."
		abend
	fi
fi

rm -f conftest.c

# Tests have been finished. Output include/config.h and Makefile
printf "creating include/config.h and Makefile... "
cat <<EOF >include/config.h
/* This file was automatically generated by configure.  */

#ifndef _CONFIG_H_
#define _CONFIG_H_

EOF

if [ "$have_flock" = "yes" ]
then
	echo "#define HAVE_LOCK_FLOCK" >>include/config.h
else
	echo "#define HAVE_LOCK_FCNTL" >>include/config.h
fi

if [ "$enable_widechar" = "yes" ]
then
	echo "#define USE_WIDECHAR" >>include/config.h
fi

if [ "$enable_threads" = "yes" ]
then
	echo "#define VI_DB_THREAD DB_THREAD" >>include/config.h
else
	echo "#define VI_DB_THREAD 0" >>include/config.h
fi

echo "" >>include/config.h

echo "#endif /* _CONFIG_H_ */" >>include/config.h

cat <<EOF >Makefile
.POSIX:
# This Makefile was automatically generated by configure.

PREFIX=$prefix
BINDIR=$bindir
DATADIR=$datadir
MANDIR=$mandir
CC=$cc
EOF

[ -n "$cflags" ] && echo "CFLAGS=$cflags" >>Makefile
[ -n "$ldflags" ] && echo "LDFLAGS=$ldflags" >>Makefile
[ -n "$libs" ] && echo "LIBS=$libs" >>Makefile

cat <<EOF >>Makefile
PROG=nvi
CATALOGS=catalog/dutch catalog/english catalog/french catalog/german \\
         catalog/ru_SU.KOI8-R catalog/spanish catalog/swedish
OBJS=cl/cl_main.o cl/cl_funcs.o cl/cl_read.o cl/cl_screen.o \\
     cl/cl_term.o \\
     common/api.o common/conv.o common/cut.o common/db.o \\
     common/delete.o common/exf.o common/gs.o common/key.o \\
     common/log.o common/main.o common/mark.o common/msg.o \\
     common/options.o common/options_f.o common/put.o \\
     common/recover.o common/screen.o common/search.o \\
     common/seq.o common/trace.o common/util.o \\
     ex/ex.o ex/ex_abbrev.o ex/ex_append.o ex/ex_args.o \\
     ex/ex_argv.o ex/ex_at.o ex/ex_bang.o ex/ex_cd.o \\
     ex/ex_cmd.o ex/ex_cscope.o ex/ex_delete.o ex/ex_display.o \\
     ex/ex_edit.o ex/ex_equal.o ex/ex_file.o ex/ex_filter.o \\
     ex/ex_global.o ex/ex_init.o ex/ex_join.o ex/ex_map.o \\
     ex/ex_mark.o ex/ex_mkexrc.o ex/ex_move.o ex/ex_open.o \\
     ex/ex_preserve.o ex/ex_print.o ex/ex_put.o ex/ex_quit.o \\
     ex/ex_read.o ex/ex_screen.o ex/ex_set.o \\
     ex/ex_shell.o ex/ex_shift.o ex/ex_source.o ex/ex_stop.o \\
     ex/ex_subst.o ex/ex_tag.o ex/ex_txt.o ex/ex_undo.o \\
     ex/ex_usage.o ex/ex_util.o ex/ex_version.o ex/ex_visual.o \\
     ex/ex_write.o ex/ex_yank.o ex/ex_z.o \\
     vi/getc.o vi/v_at.o vi/v_ch.o vi/v_cmd.o vi/v_delete.o \\
     vi/v_event.o vi/v_ex.o vi/v_increment.o vi/v_init.o \\
     vi/v_itxt.o vi/v_left.o vi/v_mark.o vi/v_match.o \\
     vi/v_paragraph.o vi/v_put.o vi/v_redraw.o vi/v_replace.o \\
     vi/v_right.o vi/v_screen.o vi/v_scroll.o vi/v_search.o \\
     vi/v_section.o vi/v_sentence.o vi/v_status.o vi/v_txt.o \\
     vi/v_ulcase.o vi/v_undo.o vi/v_util.o vi/v_word.o \\
     vi/v_xchar.o vi/v_yank.o vi/v_z.o vi/v_zexit.o \\
     vi/vi.o vi/vs_line.o vi/vs_msg.o vi/vs_refresh.o \\
     vi/vs_relative.o vi/vs_smap.o vi/vs_split.o \\
EOF

if [ "$with_regex" = "bundled" ] 
then
	cat <<EOF >>Makefile
     regex/regcomp.o regex/regerror.o regex/regexec.o regex/regfree.o \\
EOF
fi

if [ "$enable_threads" = "yes" ]
then
	echo "     common/pthread.o" >>Makefile
else
	echo "     common/nothread.o" >>Makefile
fi

cat <<EOF >>Makefile

all: \$(PROG)

\$(PROG): \$(OBJS)
	\$(CC) \$(LDFLAGS) -o \$(PROG) \$(OBJS) \$(LIBS)

cl/cl_main.o: common/options_def.h ex/ex_def.h

common/options_def.h: common/options.c
	awk -f common/options.awk common/options.c >common/options_def.h

ex/ex_def.h: ex/ex_cmd.c
	awk -f ex/ex.awk ex/ex_cmd.c >ex/ex_def.h

install: \$(PROG)
	install -d \$(DESTDIR)\$(BINDIR)
	install -c $stripflag -m 755 \$(PROG) \$(DESTDIR)\$(BINDIR)/${progprefix}vi
	install -c $stripflag -m 755 \$(PROG) \$(DESTDIR)\$(BINDIR)/${progprefix}ex
	install -c $stripflag -m 755 \$(PROG) \$(DESTDIR)\$(BINDIR)/${progprefix}view
	install -d \$(DESTDIR)\$(MANDIR)/man1
	install -c -m 644 docs/vi.man/vi.1 \$(DESTDIR)\$(MANDIR)/man1/${progprefix}vi.1
	ln -sf ${progprefix}vi.1 \$(DESTDIR)\$(MANDIR)/man1/${progprefix}ex.1
	ln -sf ${progprefix}vi.1 \$(DESTDIR)\$(MANDIR)/man1/${progprefix}view.1
	install -d \$(DESTDIR)\$(DATADIR)/vi/catalog
	install -c -m 644 \$(CATALOGS) \$(DESTDIR)\$(DATADIR)/vi/catalog
	sed -e "s#@path_preserve@#/var/preserve/vi.recover#" \\
	    -e "s#@path_sendmail@#/usr/bin/sendmail#" common/recover.in \\
	     >\$(DESTDIR)\$(DATADIR)/vi/recover
	chmod +x \$(DESTDIR)\$(DATADIR)/vi/recover

clean:
	rm -f \$(PROG) \$(OBJS) common/options_def.h ex/ex_def.h

distclean: clean
	rm -f Makefile include/config.h conftest.c
EOF
echo "done"
