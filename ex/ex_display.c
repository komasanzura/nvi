/*-
 * Copyright (c) 1992, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 * Copyright (c) 1992, 1993, 1994, 1995, 1996
 *	Keith Bostic.  All rights reserved.
 *
 * See the LICENSE file for redistribution information.
 */

#include "config.h"

#include <sys/types.h>
#include <sys/queue.h>

#include <bitstring.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>

#include "../common/common.h"
#include "tag.h"

static int	bdisplay(SCR *);
static void	db(SCR *, CB *, u_char *);

/*
 * ex_display -- :display b[uffers] | c[onnections] | s[creens] | t[ags]
 *
 *	Display cscope connections, buffers, tags or screens.
 *
 * PUBLIC: int ex_display(SCR *, EXCMD *);
 */
int
ex_display(SCR *sp, EXCMD *cmdp)
{
	switch (cmdp->argv[0]->bp[0]) {
	case L('b'):
#undef	ARG
#define	ARG	L("buffers")
		if (cmdp->argv[0]->len > STRLEN(ARG) ||
		    MEMCMP(cmdp->argv[0]->bp, ARG, cmdp->argv[0]->len))
			break;
		return (bdisplay(sp));
	case L('c'):
#undef	ARG
#define	ARG	L("connections")
		if (cmdp->argv[0]->len > STRLEN(ARG) ||
		    MEMCMP(cmdp->argv[0]->bp, ARG, cmdp->argv[0]->len))
			break;
		return (cscope_display(sp));
	case L('s'):
#undef	ARG
#define	ARG	L("screens")
		if (cmdp->argv[0]->len > STRLEN(ARG) ||
		    MEMCMP(cmdp->argv[0]->bp, ARG, cmdp->argv[0]->len))
			break;
		return (ex_sdisplay(sp));
	case L('t'):
#undef	ARG
#define	ARG	L("tags")
		if (cmdp->argv[0]->len > STRLEN(ARG) ||
		    MEMCMP(cmdp->argv[0]->bp, ARG, cmdp->argv[0]->len))
			break;
		return (ex_tag_display(sp));
	}
	ex_emsg(sp, cmdp->cmd->usage, EXM_USAGE);
	return (1);
}

/*
 * bdisplay --
 *
 *	Display buffers.
 */
static int
bdisplay(SCR *sp)
{
	CB *cbp;

	if (LIST_FIRST(&sp->wp->cutq) == NULL && sp->wp->dcbp == NULL) {
		msgq(sp, M_INFO, "123|No cut buffers to display");
		return (0);
	}

	/* Display regular cut buffers. */
	for (cbp = LIST_FIRST(&sp->wp->cutq); cbp != NULL; cbp = LIST_NEXT(cbp, q)) {
		if (ISDIGIT(cbp->name))
			continue;
		if (!TAILQ_EMPTY(&cbp->textq))
			db(sp, cbp, NULL);
		if (INTERRUPTED(sp))
			return (0);
	}
	/* Display numbered buffers. */
	for (cbp = LIST_FIRST(&sp->wp->cutq); cbp != NULL; cbp = LIST_NEXT(cbp, q)) {
		if (!ISDIGIT(cbp->name))
			continue;
		if (!TAILQ_EMPTY(&cbp->textq))
			db(sp, cbp, NULL);
		if (INTERRUPTED(sp))
			return (0);
	}
	/* Display default buffer. */
	if ((cbp = sp->wp->dcbp) != NULL)
		db(sp, cbp, "default buffer");
	return (0);
}

/*
 * db --
 *	Display a buffer.
 */
static void
db(SCR *sp, CB *cbp, u_char *name)
{
	CHAR_T *p;
	GS *gp;
	TEXT *tp;
	size_t len;

	gp = sp->gp;
	ex_printf(sp, "********** %s%s\n",
	    name == NULL ? KEY_NAME(sp, cbp->name) : name,
	    F_ISSET(cbp, CB_LMODE) ? " (line mode)" : " (character mode)");
	for (tp = TAILQ_FIRST(&cbp->textq);
	    tp; tp = TAILQ_NEXT(tp, q)) {
		for (len = tp->len, p = tp->lb; len--; ++p) {
			ex_puts(sp, KEY_NAME(sp, *p));
			if (INTERRUPTED(sp))
				return;
		}
		ex_puts(sp, "\n");
	}
}
