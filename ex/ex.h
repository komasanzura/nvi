/*-
 * Copyright (c) 1992, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 * Copyright (c) 1992, 1993, 1994, 1995, 1996
 *	Keith Bostic.  All rights reserved.
 *
 * See the LICENSE file for redistribution information.
 */

#define	PROMPTCHAR	':'		/* Prompt using a colon. */

typedef struct _excmdlist {		/* Ex command table structure. */
	CHAR_T *name;			/* Command name, underlying function. */
	int (*fn)(SCR *, EXCMD *);

#define	E_ADDR1		0x00000001	/* One address. */
#define	E_ADDR2		0x00000002	/* Two addresses. */
#define	E_ADDR2_ALL	0x00000004	/* Zero/two addresses; zero == all. */
#define	E_ADDR2_NONE	0x00000008	/* Zero/two addresses; zero == none. */
#define	E_ADDR_ZERO	0x00000010	/* 0 is a legal addr1. */
#define	E_ADDR_ZERODEF	0x00000020	/* 0 is default addr1 of empty files. */
#define	E_AUTOPRINT	0x00000040	/* Command always sets autoprint. */
#define	E_CLRFLAG	0x00000080	/* Clear the print (#, l, p) flags. */
#define	E_NEWSCREEN	0x00000100	/* Create a new screen. */
#define	E_SECURE	0x00000200	/* Permission denied if O_SECURE set. */
#define	E_VIONLY	0x00000400	/* Meaningful only in vi. */
#define	__INUSE1	0xfffff800	/* Same name space as EX_PRIVATE. */
	u_int16_t flags;

	char *syntax;			/* Syntax script. */
	char *usage;			/* Usage line. */
	char *help;			/* Help line. */
} EXCMDLIST;

#define	MAXCMDNAMELEN	12		/* Longest command name. */
extern EXCMDLIST const cmds[];		/* Table of ex commands. */

/*
 * !!!
 * QUOTING NOTE:
 *
 * Historically, .exrc files and EXINIT variables could only use ^V as an
 * escape character, neither ^Q or a user specified character worked.  We
 * enforce that here, just in case someone depends on it.
 */
#define	IS_ESCAPE(sp, cmdp, ch)						\
	(F_ISSET(cmdp, E_VLITONLY) ?					\
	    (ch) == CH_LITERAL : KEY_VAL(sp, ch) == K_VLNEXT)

/*
 * File state must be checked for each command -- any ex command may be entered
 * at any time, and most of them won't work well if a file hasn't yet been read
 * in.  Historic vi generally took the easy way out and dropped core.
 */
#define	NEEDFILE(sp, cmdp) {						\
	if ((sp)->ep == NULL) {						\
		ex_wemsg(sp, (cmdp)->cmd->name, EXM_NOFILEYET);		\
		return (1);						\
	}								\
}

/* Range structures for global and @ commands. */
typedef struct _range RANGE;
struct _range {				/* Global command range. */
	TAILQ_ENTRY(_range) q;	/* Linked list of ranges. */
	db_recno_t start, stop;		/* Start/stop of the range. */
};

/* Ex command structure. */
struct _excmd {
	LIST_ENTRY(_excmd) q;		/* Linked list of commands. */

	char	 *if_name;		/* Associated file. */
	db_recno_t	  if_lno;		/* Associated line number. */

	/* Clear the structure for the ex parser. */
#define	CLEAR_EX_PARSER(cmdp)						\
	memset(&((cmdp)->cp), 0, ((char *)&(cmdp)->flags -		\
	    (char *)&((cmdp)->cp)) + sizeof((cmdp)->flags))

	CHAR_T	 *cp;			/* Current command text. */
	size_t	  clen;			/* Current command length. */

	CHAR_T	 *save_cmd;		/* Remaining command. */
	size_t	  save_cmdlen;		/* Remaining command length. */

	EXCMDLIST const *cmd;		/* Command: entry in command table. */
	EXCMDLIST rcmd;			/* Command: table entry/replacement. */

	TAILQ_HEAD(_rh, _range) rq;	/* @/global range: linked list. */
	db_recno_t   range_lno;		/* @/global range: set line number. */
	CHAR_T	 *o_cp;			/* Original @/global command. */
	size_t	  o_clen;		/* Original @/global command length. */
#define	AGV_AT		0x01		/* @ buffer execution. */
#define	AGV_AT_NORANGE	0x02		/* @ buffer execution without range. */
#define	AGV_GLOBAL	0x04		/* global command. */
#define	AGV_V		0x08		/* v command. */
#define	AGV_ALL		(AGV_AT | AGV_AT_NORANGE | AGV_GLOBAL | AGV_V)
	u_int8_t  agv_flags;

	/* Clear the structure before each ex command. */
#define	CLEAR_EX_CMD(cmdp) {						\
	u_int32_t L__f = F_ISSET(cmdp, E_PRESERVE);			\
	memset(&((cmdp)->buffer), 0, ((char *)&(cmdp)->flags -		\
	    (char *)&((cmdp)->buffer)) + sizeof((cmdp)->flags));	\
	F_SET(cmdp, L__f);						\
}

	CHAR_T	  buffer;		/* Command: named buffer. */
	db_recno_t	  lineno;		/* Command: line number. */
	long	  count;		/* Command: signed count. */
	long	  flagoff;		/* Command: signed flag offset. */
	int	  addrcnt;		/* Command: addresses (0, 1 or 2). */
	MARK	  addr1;		/* Command: 1st address. */
	MARK	  addr2;		/* Command: 2nd address. */
	ARGS	**argv;			/* Command: array of arguments. */
	int	  argc;			/* Command: count of arguments. */

#define	E_C_BUFFER	0x00001		/* Buffer name specified. */
#define	E_C_CARAT	0x00002		/*  ^ flag. */
#define	E_C_COUNT	0x00004		/* Count specified. */
#define	E_C_COUNT_NEG	0x00008		/* Count was signed negative. */
#define	E_C_COUNT_POS	0x00010		/* Count was signed positive. */
#define	E_C_DASH	0x00020		/*  - flag. */
#define	E_C_DOT		0x00040		/*  . flag. */
#define	E_C_EQUAL	0x00080		/*  = flag. */
#define	E_C_FORCE	0x00100		/*  ! flag. */
#define	E_C_HASH	0x00200		/*  # flag. */
#define	E_C_LIST	0x00400		/*  l flag. */
#define	E_C_PLUS	0x00800		/*  + flag. */
#define	E_C_PRINT	0x01000		/*  p flag. */
	u_int16_t iflags;		/* User input information. */

#define	__INUSE2	0x000007ff	/* Same name space as EXCMDLIST. */
#define	E_BLIGNORE	0x00000800	/* Ignore blank lines. */
#define	E_NAMEDISCARD	0x00001000	/* Free/discard the name. */
#define	E_NOAUTO	0x00002000	/* Don't do autoprint output. */
#define	E_NOPRDEF	0x00004000	/* Don't print as default. */
#define	E_NRSEP		0x00008000	/* Need to line adjust ex output. */
#define	E_OPTNUM	0x00010000	/* Number edit option affected. */
#define	E_VLITONLY	0x00020000	/* Use ^V quoting only. */
#define	E_PRESERVE	0x0003f800	/* Bits to preserve across commands. */

#define	E_ABSMARK	0x00040000	/* Set the absolute mark. */
#define	E_ADDR_DEF	0x00080000	/* Default addresses used. */
#define	E_DELTA		0x00100000	/* Search address with delta. */
#define	E_MODIFY	0x00200000	/* File name expansion modified arg. */
#define	E_MOVETOEND	0x00400000	/* Move to the end of the file first. */
#define	E_NEWLINE	0x00800000	/* Found ending <newline>. */
#define	E_SEARCH_WMSG	0x01000000	/* Display search-wrapped message. */
#define	E_USELASTCMD	0x02000000	/* Use the last command. */
#define	E_VISEARCH	0x04000000	/* It's really a vi search command. */
#define	E_MOVETOEOL	0x08000000	/* Move to the last character of the line. */
	u_int32_t flags;		/* Current flags. */
};

/* Ex private, per-screen memory. */
typedef struct _ex_private {
	TAILQ_HEAD(_tqh, _tagq) tq;	/* Tag queue. */
	TAILQ_HEAD(_tagfh, _tagf) tagfq;/* Tag file list. */
	LIST_HEAD(_csch, _csc) cscq;    /* Cscope connection list. */
	CHAR_T	*tag_last;		/* Saved last tag string. */

	CHAR_T	*lastbcomm;		/* Last bang command. */

	ARGS   **args;			/* Command: argument list. */
	int	 argscnt;		/* Command: argument list count. */
	int	 argsoff;		/* Command: offset into arguments. */

	u_int32_t fdef;			/* Saved E_C_* default command flags. */

	char	*ibp;			/* File line input buffer. */
	size_t	 ibp_len;		/* File line input buffer length. */
	CONVWIN	 ibcw;			/* File line input conversion buffer. */

	/*
	 * Buffers for the ex output.  The screen/vi support doesn't do any
	 * character buffering of any kind.  We do it here so that we're not
	 * calling the screen output routines on every character.
	 *
	 * XXX
	 * Change to grow dynamically.
	 */
	char	 obp[1024];		/* Ex output buffer. */
	size_t	 obp_len;		/* Ex output buffer length. */

#define	EXP_CSCINIT	0x01		/* Cscope initialized. */
	u_int8_t flags;
} EX_PRIVATE;
#define	EXP(sp)	((EX_PRIVATE *)((sp)->ex_private))

/*
 * Filter actions:
 *
 *	FILTER_BANG	!:	filter text through the utility.
 *	FILTER_RBANG	!:	read from the utility (without stdin).
 *	FILTER_READ	read:	read from the utility (with stdin).
 *	FILTER_WRITE	write:	write to the utility, display its output.
 */
enum filtertype { FILTER_BANG, FILTER_RBANG, FILTER_READ, FILTER_WRITE };

/* Ex common error messages. */
typedef enum {
	EXM_EMPTYBUF,			/* Empty buffer. */
	EXM_FILECOUNT,			/* Too many file names. */
	EXM_LOCKED,			/* Another thread is active. */
	EXM_NOCANON,			/* No terminal interface. */
	EXM_NOCANON_F,			/* EXM_NOCANO: filter version. */
	EXM_NOFILEYET,			/* Illegal until a file read in. */
	EXM_NOPREVBUF,			/* No previous buffer specified. */
	EXM_NOPREVRE,			/* No previous RE specified. */
	EXM_NOSUSPEND,			/* No suspension. */
	EXM_SECURE,			/* Illegal if secure edit option set. */
	EXM_SECURE_F,			/* EXM_SECURE: filter version */
	EXM_USAGE			/* Standard usage message. */
} exm_t;

/* Ex address error types. */
enum badaddr { A_COMBO, A_EMPTY, A_EOF, A_NOTSET, A_ZERO };

/* Ex common tag error messages. */                                         
typedef enum {
	TAG_BADLNO,		/* Tag line doesn't exist. */
	TAG_EMPTY,		/* Tags stack is empty. */
	TAG_SEARCH		/* Tags search pattern wasn't found. */
} tagmsg_t;

#include "ex_def.h"

/* ex.c */
int ex(SCR **);
int ex_cmd(SCR *);
int ex_range(SCR *, EXCMD *, int *);
int ex_is_abbrev(SCR *, CHAR_T *, size_t);
int ex_is_unmap(SCR *, CHAR_T *, size_t);
void ex_badaddr(SCR *, EXCMDLIST const *, enum badaddr, enum nresult);

/* ex_abbrev.c */
int ex_abbr(SCR *, EXCMD *);
int ex_unabbr(SCR *, EXCMD *);

/* ex_append.c */
int ex_append(SCR *, EXCMD *);
int ex_change(SCR *, EXCMD *);
int ex_insert(SCR *, EXCMD *);

/* ex_args.c */
int ex_next(SCR *, EXCMD *);
int ex_prev(SCR *, EXCMD *);
int ex_rew(SCR *, EXCMD *);
int ex_args(SCR *, EXCMD *);
char **ex_buildargv(SCR *, EXCMD *, char *);

/* ex_argv.c */
int argv_init(SCR *, EXCMD *);
int argv_exp0(SCR *, EXCMD *, CHAR_T *, size_t);
int argv_exp1(SCR *, EXCMD *, CHAR_T *, size_t, int);
int argv_exp2(SCR *, EXCMD *, CHAR_T *, size_t);
int argv_exp3(SCR *, EXCMD *, CHAR_T *, size_t);
int argv_free(SCR *);

/* ex_at.c */
int ex_at(SCR *, EXCMD *);

/* ex_bang.c */
int ex_bang(SCR *, EXCMD *);

/* ex_cd.c */
int ex_cd(SCR *, EXCMD *);

/* ex_cscope.c */
int ex_cscope(SCR *, EXCMD *);
int cscope_display(SCR *);
int cscope_search(SCR *, TAGQ *, TAG *);

/* ex_delete.c */
int ex_delete(SCR *, EXCMD *);

/* ex_display.c */
int ex_display(SCR *, EXCMD *);

/* ex_edit.c */
int ex_edit(SCR *, EXCMD *);

/* ex_equal.c */
int ex_equal(SCR *, EXCMD *);

/* ex_file.c */
int ex_file(SCR *, EXCMD *);

/* ex_filter.c */
int ex_filter(SCR *, EXCMD *, MARK *, MARK *, MARK *, CHAR_T *, enum filtertype);

/* ex_global.c */
int ex_global(SCR *, EXCMD *);
int ex_v(SCR *, EXCMD *);
int ex_g_insdel(SCR *, lnop_t, db_recno_t);

/* ex_init.c */
int ex_screen_copy(SCR *, SCR *);
int ex_screen_end(SCR *);
int ex_optchange(SCR *, int, char *, u_long *);
int ex_exrc(SCR *);
int ex_run_str(SCR *, char *, CHAR_T *, size_t, int, int);

/* ex_join.c */
int ex_join(SCR *, EXCMD *);

/* ex_map.c */
int ex_map(SCR *, EXCMD *);
int ex_unmap(SCR *, EXCMD *);

/* ex_mark.c */
int ex_mark(SCR *, EXCMD *);

/* ex_mkexrc.c */
int ex_mkexrc(SCR *, EXCMD *);

/* ex_move.c */
int ex_copy(SCR *, EXCMD *);
int ex_move(SCR *, EXCMD *);

/* ex_open.c */
int ex_open(SCR *, EXCMD *);

/* ex_preserve.c */
int ex_preserve(SCR *, EXCMD *);
int ex_recover(SCR *, EXCMD *);

/* ex_print.c */
int ex_list(SCR *, EXCMD *);
int ex_number(SCR *, EXCMD *);
int ex_pr(SCR *, EXCMD *);
int ex_print(SCR *, EXCMD *, MARK *, MARK *, u_int32_t);
int ex_ldisplay(SCR *, const CHAR_T *, size_t, size_t, u_int);
int ex_scprint(SCR *, MARK *, MARK *);
int ex_printf(SCR *, const char *, ...);
int ex_puts(SCR *, const char *);
int ex_fflush(SCR *sp);

/* ex_put.c */
int ex_put(SCR *, EXCMD *);

/* ex_quit.c */
int ex_quit(SCR *, EXCMD *);

/* ex_read.c */
int ex_read(SCR *, EXCMD *);
int ex_readfp(SCR *, char *, FILE *, MARK *, db_recno_t *, int);

/* ex_screen.c */
int ex_bg(SCR *, EXCMD *);
int ex_fg(SCR *, EXCMD *);
int ex_resize(SCR *, EXCMD *);
int ex_sdisplay(SCR *);

/* ex_set.c */
int ex_set(SCR *, EXCMD *);

/* ex_shell.c */
int ex_shell(SCR *, EXCMD *);
int ex_exec_proc(SCR *, EXCMD *, char *, const char *, int);
int proc_wait(SCR *, long, const char *, int, int);

/* ex_shift.c */
int ex_shiftl(SCR *, EXCMD *);
int ex_shiftr(SCR *, EXCMD *);

/* ex_source.c */
int ex_source(SCR *, EXCMD *);

/* ex_stop.c */
int ex_stop(SCR *, EXCMD *);

/* ex_subst.c */
int ex_s(SCR *, EXCMD *);
int ex_subagain(SCR *, EXCMD *);
int ex_subtilde(SCR *, EXCMD *);
int re_compile(SCR *, CHAR_T *, size_t, CHAR_T **, size_t *, regex_t *, u_int);
void re_error(SCR *, int, regex_t *);

/* ex_tag.c */
int ex_tag_first(SCR *, CHAR_T *);
int ex_tag_push(SCR *, EXCMD *);
int ex_tag_next(SCR *, EXCMD *);
int ex_tag_prev(SCR *, EXCMD *);
int ex_tag_nswitch(SCR *, TAG *, int);
int ex_tag_Nswitch(SCR *, TAG *, int);
int ex_tag_pop(SCR *, EXCMD *);
int ex_tag_top(SCR *, EXCMD *);
int ex_tag_display(SCR *);
int ex_tag_copy(SCR *, SCR *);
int tagq_free(SCR *, TAGQ *);
int tagq_push(SCR*, TAGQ*, int, int );
void tag_msg(SCR *, tagmsg_t, char *);
int ex_tagf_alloc(SCR *, char *);
int ex_tag_free(SCR *);

/* ex_txt.c */
int ex_txt(SCR *, TEXTH *, ARG_CHAR_T, u_int32_t);

/* ex_undo.c */
int ex_undo(SCR *, EXCMD *);

/* ex_usage.c */
int ex_help(SCR *, EXCMD *);
int ex_usage(SCR *, EXCMD *);
int ex_viusage(SCR *, EXCMD *);

/* ex_util.c */
void ex_cinit(SCR *, EXCMD *, int, int, db_recno_t, db_recno_t, int);
int ex_getline(SCR *, FILE *, size_t *, int *);
int ex_ncheck(SCR *, int);
int ex_init(SCR *);
void ex_wemsg(SCR *, CHAR_T *, exm_t);
void ex_emsg(SCR *, char *, exm_t);

/* ex_version.c */
int ex_version(SCR *, EXCMD *);

/* ex_visual.c */
int ex_visual(SCR *, EXCMD *);

/* ex_write.c */
int ex_wn(SCR *, EXCMD *);
int ex_wq(SCR *, EXCMD *);
int ex_write(SCR *, EXCMD *);
int ex_xit(SCR *, EXCMD *);
int ex_writefp(SCR *, char *, FILE *, MARK *, MARK *, u_long *, u_long *, int);

/* ex_yank.c */
int ex_yank(SCR *, EXCMD *);

/* ex_z.c */
int ex_z(SCR *, EXCMD *);
